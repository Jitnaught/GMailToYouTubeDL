﻿using System.Diagnostics;
using Gtk;

namespace GMailToYouTubeDL
{
	public class MessageBox
	{
		public static ResponseType Show(string msg, MessageType messageType, ButtonsType buttons)
		{
			Debug.WriteLine(msg);
			MessageDialog md = new MessageDialog(null, DialogFlags.Modal, messageType, buttons, msg);
			ResponseType result = (ResponseType)md.Run();

			md.Destroy();

			return result;
		}

		public static void Show(string msg)
		{
			Debug.WriteLine(msg);
			MessageDialog md = new MessageDialog(null, DialogFlags.Modal, MessageType.Other, ButtonsType.Ok, msg);
			md.Run();
			md.Destroy();
		}
	}
}
