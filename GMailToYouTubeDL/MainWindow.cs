﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using GMailToYouTubeDL;
using Gtk;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;

public partial class MainWindow : Gtk.Window
{
    private const string START_YT_LINK = "http://www.youtube.com/watch?v=";
    private const string START_YT_ID = "watch?v=";
    private const string REGEXP_EMAIL = @"^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$";
    private const string YTDL_FORMATS_LINK = "https://github.com/rg3/youtube-dl/blob/master/README.md#format-selection";

    private ImapClient client = null;

    private List<string> youtubeIDs = new List<string>();
    private List<UniqueId> uids = new List<UniqueId>();

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    private void enableWidgets(bool enabled)
    {
        tvEmail.Sensitive = enabled;
        tvPassword.Sensitive = enabled;
        btnGetEmails.Sensitive = enabled;
        tvFormats.Sensitive = enabled;
        tvLocation.Sensitive = enabled;
        btnBrowseLocation.Sensitive = enabled;
        btnDownload.Sensitive = enabled;
        btnTrash.Sensitive = enabled;
    }

    private void openClient()
    {
        client = new ImapClient();

        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
        client.Connect("imap.gmail.com", 993, true);
        client.AuthenticationMechanisms.Remove("XOAUTH2");

        while (true)
        {
            try
            {
                client.Authenticate(tvEmail.Buffer.Text, tvPassword.Buffer.Text);
            }
            catch (Exception)
            {
                if (MessageBox.Show("An error occured logging in. Try again?", MessageType.Error, ButtonsType.YesNo) == ResponseType.Yes)
                    continue;

                enableWidgets(true);
                this.GdkWindow.Cursor = null;
                return;
            }

            break;
        }
    }

    private void exitClient()
    {
        if (client != null)
        {
            if (client.IsAuthenticated && client.Inbox.IsOpen) client.Inbox.Close();
            if (client.IsConnected) client.Disconnect(true);
        }

        client = null;
    }

    private void log(string message)
    {
        tvVideoList.Buffer.Text += message + Environment.NewLine;
    }

    private void runYTDL(string command)
    {
        int plat = (int)Environment.OSVersion.Platform;

        if ((plat != 4) && (plat != 128)) //windows/mac
        {
            Process.Start("youtube-dl", command);
        }
        else //unix
        {
            ProcessStartInfo procInfo = new ProcessStartInfo();
            procInfo.WindowStyle = ProcessWindowStyle.Normal;
            procInfo.UseShellExecute = false;
            procInfo.FileName = "gnome-terminal";
            procInfo.Arguments = "-e \"youtube-dl " + command + "\"";
            Process.Start(procInfo);
        }
    }

    protected void btnGetEmails_Clicked(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(tvEmail.Buffer.Text) || !Regex.IsMatch(tvEmail.Buffer.Text, REGEXP_EMAIL))
        {
            MessageBox.Show("Email is not correct format.");
            return;
        }

        if (string.IsNullOrWhiteSpace(tvPassword.Buffer.Text))
        {
            MessageBox.Show("Password is empty.");
            return;
        }

        enableWidgets(false);
        this.GdkWindow.Cursor = new Gdk.Cursor(Gdk.CursorType.Watch);

        youtubeIDs.Clear();
        tvVideoList.Buffer.Clear();
        uids.Clear();

        exitClient();

        openClient();

        var inbox = client.Inbox;
        inbox.Open(FolderAccess.ReadOnly);

        var msgIds = inbox.Search(SearchQuery.NotDeleted.And(SearchQuery.SubjectContains("just uploaded a video")));

        if (msgIds.Count > 0)
        {
            foreach (var msgId in msgIds)
            {
                var subject = "Email UID: " + msgId.Id.ToString();

                try
                {
                    var msg = inbox.GetMessage(msgId);

                    subject = msg.Subject;

                    if (subject.StartsWith("Re:") || subject.StartsWith("Fwd:")) continue;

                    var body = HttpUtility.UrlDecode(msg.HtmlBody);

                    int indexOfYTLink = body.IndexOf(START_YT_ID);
                    if (indexOfYTLink == -1)
                    {
                        log("Error with email: " + subject);
                        continue;
                    }

                    int startIndex = indexOfYTLink + START_YT_ID.Length;

                    int endIndex = body.Substring(startIndex).IndexOf("&feature=");

                    if (endIndex == -1)
                    {
                        log("Error with email: " + subject);
                        continue;
                    }

                    string id = body.Substring(startIndex, endIndex);

                    youtubeIDs.Add(id);
                    uids.Add(msgId);

                    log("Subject: " + subject + ", ID: " + id);
                }
                catch (Exception)
                {
                    log("Error with email: " + subject);
                }
            }
        }
        else
        {
            MessageBox.Show("No YouTube video emails found.");
            exitClient();
            enableWidgets(true);
            this.GdkWindow.Cursor = null;
            return;
        }

        if (youtubeIDs.Count == 0)
        {
            MessageBox.Show("No YouTube video emails found.");
        }

        enableWidgets(true);
        this.GdkWindow.Cursor = null;
    }

    protected void btnBrowseLocation_Clicked(object sender, EventArgs e)
    {
        var dialog = new FileChooserDialog("Select folder", null, FileChooserAction.SelectFolder);

        dialog.AddButton(Stock.Cancel, ResponseType.Cancel);
        dialog.AddButton(Stock.Open, ResponseType.Ok);

        if ((ResponseType)dialog.Run() == ResponseType.Ok)
        {
            tvLocation.Buffer.Text = dialog.Filename;
        }

        dialog.Destroy();
    }

    protected void btnDownload_Clicked(object sender, EventArgs e)
    {
        if (youtubeIDs.Count > 0)
        {
            if (string.IsNullOrWhiteSpace(tvFormats.Buffer.Text))
            {
                MessageBox.Show("No formats entered.");
                return;
            }

            if (string.IsNullOrWhiteSpace(tvLocation.Buffer.Text) || !Directory.Exists(tvLocation.Buffer.Text))
            {
                MessageBox.Show("Download location not found.");
                return;
            }

            var command = "-f " + tvFormats.Buffer.Text + " --ignore-errors --output \\\"" + tvLocation.Buffer.Text + (tvLocation.Buffer.Text.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) ? "" : System.IO.Path.DirectorySeparatorChar.ToString()) + "%(title)s.%(ext)s\\\" ";

            foreach (string id in youtubeIDs) command += START_YT_LINK + id + " ";

            try
            {
                runYTDL(command);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }

    protected void btnTrash_Clicked(object sender, EventArgs e)
    {
        if (uids.Count > 0 && MessageBox.Show("Do you really want to delete the " + uids.Count.ToString() + " emails?", MessageType.Warning, ButtonsType.YesNo) == ResponseType.Yes)
        {
            if (client == null || !client.IsConnected || !client.IsAuthenticated)
            {
                exitClient();

                openClient();
            }

            if (client.Inbox.IsOpen && client.Inbox.Access != FolderAccess.ReadWrite) client.Inbox.Close();

            if (!client.Inbox.IsOpen) client.Inbox.Open(FolderAccess.ReadWrite);

            client.Inbox.MoveTo(uids, client.GetFolder(SpecialFolder.Trash));

            exitClient();

            MessageBox.Show("Deleted " + uids.Count.ToString() + " emails");

            uids.Clear();

            tvVideoList.Buffer.Clear();
        }
    }

    protected void lblFormats_ButtonPressEvent(object o, ButtonPressEventArgs args)
    {
        Process.Start(YTDL_FORMATS_LINK);
    }
}
