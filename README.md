A simple program made in C# using Mono (.NET Framework 4.5), GTK# 2.12, and MailKit that gets YouTube upload notification emails from GMail and sends them off to youtube-dl.

If you use two-factor authentication you need to [create an app password](https://security.google.com/settings/security/apppasswords) and use that instead of your normal password.

To use, enter your email and (app) password, then click "Get emails." This will grab all of your YouTube notification emails in your inbox. If you have a lot of emails it may take a while.
Next you have to change the format(s) to what you want to download, then select the directory you want to download the videos to.
Once you've done all of that you can click "Download videos." This will open a terminal/command prompt 

Requirements:
* youtube-dl (if running Windows make sure to put youtube-dl.exe in this program's directory, or set an environment variable to where youtube-dl is)
* Linux: mono, gtk-sharp2, gnome-terminal (since that is what is used to open youtube-dl in a terminal)
* Windows: .NET Framework 4.5 & [GTK#](http://www.mono-project.com/download/#download-win)
* Mac: (untested) [Mono](http://www.mono-project.com/download/#download-mac)

[Download.](https://gitlab.com/Jitnaught/GMailToYouTubeDL/tags)

P.S. If you are building from source using MonoDevelop on Linux make sure to remove the Mono.Posix reference, otherwise the program won't run under Windows.

![Main window](https://i.imgur.com/pueH3uW.jpg)
